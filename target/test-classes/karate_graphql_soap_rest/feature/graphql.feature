@debug
Feature: Verify Coffees

  Background:
    Given url 'http://localhost:8080/graphql'

  Scenario: feature: Get all coffees
    Given path '/'
    Given text query =
    """
      query{
        findAll{
          id,name
        }
      }
    """
    And request { query: '#(query)' }
    When method post
    Then status 200
    Then match response == '#object'
